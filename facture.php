<?php
//numéro de facture aléatoire 
function num_fact($taille)
{
    $num = '01234567890123456789';
    $longueurmax = strlen($num);
    
    for($i = 0; $i < $taille; $i++)
    {
        $random .= $num[mt_rand(0, $longueurmax)];
    }

    return($random);
}

//tableau des informations de l'entreprise
$entreprise = array (
    array(
        "name" => '<h1 class="npng">NO PAIN NO GAIN FITNESS</h1>',
        "adresse" => '<p>7/9 rue de jean prouvé</p>',
        "poste" => '<p>94800 Villejuif</p>',
        "tel" => '<p><span class="underline">Tel :</span> 01.34.75.00.11</p>',
        "mail" => '<p><span class="underline">Mail :</span><a class="deco" href=\"mailto:nopain.nogain@gmail.com\" > nopain.nogain@gmail.com </a></p>'
    )
    );

//tableau associatif des informations des clients
//il y a plusieurs clients pour varier les possibilités lors de cette exercice 
$client = array (
    array (
        "name" => 'M. Cooper',
        "adresse" => '5 rue des monts',
        "poste" => '74000 Annecy'
    ),
    array (
        "name" => 'Mme. Olsen',
        "adresse" => '72 rue du capitol',
        "poste" => '75002 Paris'
    ),
    array (
        "name" => 'M. Alves',
        "adresse" => '19 rue des forts',
        "poste" => '33300 Bordeaux'
    )
);

//lieu aléatoire pour varier les possibilités lors de cette exercice 
$lieu = ['Paris','Lyon','Nice','Marseille','Toulouse','Nantes','Starsbourg','Montpellier','Bordeaux','Rennes'];
$randlieu = rand(0, 9);

//date du jour et heure variant en fonction de l'instant 
date_default_timezone_set("Europe/Paris"); //défnition du fuseau horaire

function recup_mois() //récupération du mois 
{
    $mois_fr = array (
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Decembre"
    );

    $index = date("m") - 1;

    return($mois_fr[$index]);
}

function recup_jours() //récupération du jours
{
    $jours_fr = array(
        "Dimanche",
        "Lundi",
        "Mardi",
        "Mercredi",
        "Jeudi",
        "Vendredi",
        "Samedi"
    );

    $index = date("w");

    return($jours_fr[$index]);
}

$num = date("d");
$jour = recup_jours();
$mois = recup_mois();
$annee = date("Y"); 

$heure = date("H:i:s");

//tableau indexé numériquement 
$box = array('Référence','Libellé','Prix unitaire','Quantité','Prix HT'); 

//tableau associatif 
$information = array (
array (
    "ref" => num_fact($taille = 15),
    "libel" => "Sweat TRAP-X - noir",
    "prix unitaire" => 44,
    "quant" => mt_rand(1, 50), //quantité aléatoire pour faire varier les prix 
    "prix ht" => ''
),
array (
    "ref" => num_fact($taille = 15),
    "libel" => "Jogging Fleece Noir/Blanc",
    "prix unitaire" => 40,
    "quant" => mt_rand(1, 50), //quantité aléatoire pour faire varier les prix 
    "prix ht" => ''
),
array (
    "ref" => num_fact($taille = 15),
    "libel" => "Baseball Jersey NPNG Noir",
    "prix unitaire" => 25,
    "quant" => mt_rand(1, 50), //quantité aléatoire pour faire varier les prix 
    "prix ht" => ''
),
array (
    "ref" => num_fact($taille = 15),
    "libel" => "T-shirt Reckless Double Camo",
    "prix unitaire" => 25,
    "quant" => mt_rand(1, 50), //quantité aléatoire pour faire varier les prix 
    "prix ht" => ''
),
array (
    "ref" => num_fact($taille = 15),
    "libel" => "T-shirt Identity",
    "prix unitaire" => 21,
    "quant" => mt_rand(1, 50), //quantité aléatoire pour faire varier les prix 
    "prix ht" => ''
)
);

//Prix TTC
function calcul_ttc($htmax)
{
    $prixttc = $htmax * 1.2; // 1.2 pour 20%

    return($prixttc);
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="./images/facture.png"/>
    <link rel="stylesheet" href="./styles/reset.css">
    <link rel="stylesheet" href="./styles/styles.css">
    <title>Exercice 25- Facture</title>
</head>
<body>
    <section class="top">
        <div class="container-top">
            <div>
                <img class="logo" src="./images/logo.png" alt="">
            </div>
            <div class="info-box-one">
                <?php
                echo($entreprise[0]["name"]);
                echo($entreprise[0]["adresse"]);
                echo($entreprise[0]["poste"]);
                echo($entreprise[0]["tel"]);
                echo($entreprise[0]["mail"]);
                ?>
            </div>
        </div>
        <div class="info-box-two">
            <?php
            echo($client[mt_rand(0, 2)]["name"]);
            echo("<br>");
            echo($client[mt_rand(0, 2)]["adresse"]);
            echo("<br>");
            echo($client[mt_rand(0, 2)]["poste"]);
            ?>
        </div>
        <div class="info-box-three">
            <?php
            echo("<p class=\"facture\">");
            echo("Facture n° ".num_fact($taille = 10));
            echo("</p>");
            echo("<br>");
            echo("édité le ".$jour." ".$num." ".$mois." ".$annee." à ".$heure);
            echo("<br><br>");
            echo ("dans la ville de ".$lieu[$randlieu]);
            ?>
        </div>
    </section>
    <table>
        <?php
            echo("<tr>");
            foreach ($box as $valeur)
            {
                echo ("<th>".$valeur."</th>");
            }

            /* autre maniere de faire sans le foreach 
            for ($i = 0; $i < count($box); $i++) 
            {
                echo("<th>".$box[$i]."</th>");
            }
            */
            echo("</tr>");

            foreach ($information as $valeur)
            {
                echo("<tr>");
                echo("<td>".$valeur["ref"]."</td>");
                echo("<td>".$valeur["libel"]."</td>");
                echo("<td>".$valeur["prix unitaire"].",00€</td>");
                echo("<td>".$valeur["quant"]."</td>");
                echo("<td>".$valeur["prix unitaire"]*$valeur["quant"]."€</td>");
                echo("</tr>");
                $htmax += $valeur["prix unitaire"]*$valeur["quant"];
            }

            /* autre maniere de faire sans le foreach 
            for ($i = 0; $i < count($information); $i++) 
            {
                echo("<tr>");
                echo("<td>".$information[$i]["ref"]."</td>");
                echo("<td>".$information[$i]["libel"]."</td>");
                echo("<td>".$information[$i]["prix unitaire"].",00€</td>");
                echo("<td>".$information[$i]["quant"]."</td>");
                echo("<td>".$information[$i]["prix unitaire"]*$information[$i]["quant"]."€</td>");
                echo("</tr>");
                $htmax += $information[$i]["prix unitaire"]*$information[$i]["quant"];
            }
            */
        ?>
    </table>
    <section>
        <div class="ht-total">
            <?php
            echo("<span class=\"strong\">Total HT :</span> ".$htmax."€")
            ?>
        </div>
        <div class="tva">
            <?php
            echo("<span class=\"strong\">TVA :</span> 20%");
            ?>
        </div>
        <div class="ttc">
            <?php
            echo("<span class=\"strong\">Prix TTC :</span> ".calcul_ttc($htmax)."€");
            ?>
        </div>
    </section>
    <section class="bot">
        <input type="button" value="Imprimer" onClick="window.print()" class="doc">
        <div class="white-bar"></div>
        <img class="badge" src="./images/logo2.png" alt="">
        <div class="foot">
            <p class="informations">
                Conditions de paiement : paiement à réception de facture, à 30 jours...<br>
                Aucun escompte consenti pour règlement anticipé<br>
                Tout incident de paiement est passible d'intérêt de retard. Le montant des pénalités résulte de l'application aux sommes restant dues d'un taux d'intérêt légal en vigueur au moment de l'incident.<br>
                Indemnité forfaitaire pour frais de recouvrement due au créancier en cas de retard de paiement: 40€
            </p>
            <p class="code">
                N° Siret 210.896.764 00015 RCS Montpellier<br>
                Code APE 947A - N° TVA Intracom. FR 77825896764000
            </p>
        </div>
    </section>
</body>
</html>